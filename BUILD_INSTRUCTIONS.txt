################## Install needed dependencies (assumes running on Ubuntu 16.04) ##################
sudo apt-get install pandoc python-setuptools python3-setuptools twine devscripts
sudo pip install setuptools-markdown
# ensure you're running by default Python 2.7
python --version

################## setup ~/.pypirc  with credentials for PyPi and TestPyPi ##################

[distutils]
index-servers =
  pypi
  pypitest

[pypi]
repository=https://pypi.python.org/pypi
username=USERNAME
password=TEST_PASSWORD

[pypitest]
repository=https://testpypi.python.org/pypi
username=USERNAME
password=PROD_PASSWORD

################## register package with TestPyPi and PyPi, needed only once(first time only) ##################
python setup.py register -r pypitest
python setup.py register -r pypi

################## adjust version and download_url in setup.py ##################

################## build PyPi package and upload it , VERSION needs to match whaterver you set in setup.py ##################
export VERSION=`grep '^    version' setup.py  | awk -F\' {'print $2'}`
# build package (wheel dist)
rm -f dist/s3backuptool-*
python setup.py bdist_wheel

# upload to Test PyPi https://testpypi.python.org/
twine upload dist/s3backuptool-${VERSION}-py2-none-any.whl -r pypitest

# install test version from TestPyPi
sudo pip install -i https://testpypi.python.org/pypi s3backuptool==${VERSION}


# upload to PyPi
twine upload dist/s3backuptool-${VERSION}-py2-none-any.whl -r pypi
# install release version from PyPi
sudo pip install s3backuptool==${VERSION}

################# build packages for RedHat and Debian based distributions ###########

To make a package for a new version do:
- adjust the Version and Changelog lines in package_build/s3backuptool.spec
- adjust the version in package_build/s3backuptool.dsc and also adjust the Debian Changelog by running:
	cd package_build
	mkdir -p debian
	ln -s debian.changelog debian/changelog
	# run debchange and add the changelog for the new release
	debchange
	rm -rf debian

- upload:
	- the source (tar.gz compressed, named in format:  version.tar.gz ). This is normally obtained from https://bitbucket.org/alexandru_ionica/s3backuptool/get/%{version}.tar.gz given that a release was properly tagged via Git
	- all files from package_build/  to https://build.opensuse.org
