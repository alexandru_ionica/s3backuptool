from setuptools import setup
setup(
    setup_requires=['setuptools-markdown'],
    name = 's3backuptool',
    packages = ['s3backuptool'],  # this must be the same as the name above
    version = '0.1.2',
    description = 'Command line backup tool which backups local files to Amazon S3. AES-256 client side encryption support is also available. Only regular '
                  'files, symlinks and folders are backed up.',
    long_description_markdown_filename='README.md',  # try to convert README.md to reStructuredText and include it inline
    author = 'Alexandru Ionica',
    author_email = 'alexandru@ionica.eu',
    license='GPLv2',
    url = 'https://bitbucket.org/alexandru_ionica/s3backuptool',  # use the URL to the github repo
    download_url = 'https://bitbucket.org/alexandru_ionica/s3backuptool/get/0.1.2.tar.gz',  # I'll explain this in a second
    keywords = 's3backup s3backuptool',  # arbitrary keywords
    install_requires=['boto', 'pycrypto'],
    classifiers=[
        # How mature is this project? Common values are
        #   3 - Alpha
        #   4 - Beta
        #   5 - Production/Stable
        'Development Status :: 3 - Alpha',

        # Indicate who your project is intended for
        'Intended Audience :: Developers',
        'Intended Audience :: System Administrators',

        'Topic :: System :: Archiving :: Backup',

        # Pick your license as you wish (should match "license" above)
        'License :: OSI Approved :: GNU General Public License v2 (GPLv2)',

        'Programming Language :: Python :: 2.7',
    ],
    # data_files=[('/usr/bin/', ['s3backuptool/s3backuptool.py'])],
    scripts=['s3backuptool/s3backuptool.py'],
)
