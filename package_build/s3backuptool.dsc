Format: 1.0
Source: s3backuptool
Version: 0.1.2-1
Binary: s3backuptool
Maintainer: Alexandru Ionica <alexandru@ionica.eu>
Homepage: https://bitbucket.org/alexandru_ionica/s3backuptool
Architecture: any
Section: contrib/utils
Priority: extra
Build-Depends: debhelper (>= 4.1.16)
Files: 
 4966b7a3c97381b09847347535a48ddd 53904 0.1.2.tar.gz
