In this folder you have the files needed to build packages for various Linux Distributions via Suse's Open Build System

The files are needed for: 
debian.changelog   - Debian and Ubuntu Builds
debian.control     - Debian and Ubuntu Builds
debian.rules       - Debian and Ubuntu Builds
s3backuptool.dsc   - Debian and Ubuntu Builds
s3backuptool.spec  - RedHat/CentOS/Fedora/Scientific Linux

Beside the above also the source (tar.gz compressed, named in format:  version.tar.gz ) needs to be uploaded. This is normally obtained from https://bitbucket.org/alexandru_ionica/s3backuptool/get/%{version}.tar.gz given that a release was properly tagged via Git
