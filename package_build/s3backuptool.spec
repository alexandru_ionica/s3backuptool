Name:           s3backuptool
Version:        0.1.2
Release:        0
License:        GPLv2
Summary:        Backup local files to Amazon S3
Url:            https://bitbucket.org/alexandru_ionica/s3backuptool
Group:          Development/Languages/Python
BuildArch:      noarch
Requires:       python python-crypto python-boto
Source0:        https://bitbucket.org/alexandru_ionica/s3backuptool/get/%{version}.tar.gz
BuildArch: 	noarch

%description
s3backuptool - tool to backup local files to Amazon S3
Project home is https://bitbucket.org/alexandru_ionica/s3backuptool

%prep
%setup -c "%{version}"

%install
rm -rf "%{buildroot}"
mkdir "%{buildroot}"
mkdir -p "%{buildroot}/usr/local/bin"
cp ${RPM_BUILD_DIR}/%{name}-%{version}/*-s3backuptool-*/s3backuptool/s3backuptool.py "%{buildroot}/usr/local/bin/"
chmod +x "%{buildroot}/usr/local/bin/s3backuptool.py"

%files
/usr/local/bin/s3backuptool.py

%changelog
* Fri Dec 23 2016 Alexandru Ionica <alexandru@ionica.eu>
- added option to decrypt file without having to setup a config file first
