#!/usr/bin/env python

import sys
sys.path.append("../s3backuptool") # Adds higher directory to python modules path.
sys.path.append("./s3backuptool") # Adds higher directory to python modules path
import unittest
import warnings
import argparse
from s3backuptool import *
from mock import patch
from pprint import pprint
import logging
import ConfigParser
import os
import stat
import hashlib
import base64
import sqlite3
import re
import boto.s3.connection
import boto.s3.key
import math
import io
import struct
import datetime
import time
from Crypto.Protocol.KDF import PBKDF2
from Crypto.Cipher import AES
from pprint import pprint


def get_args():
    """ Get arguments from CLI """

    parser = argparse.ArgumentParser(description='Script which performs unit tests')
    parser.add_argument('-v', '--verbose', required=False, action="store_true", default=False, help='Show verbose level messages')
    args = parser.parse_args()
    return args

class test_CallbackProgress_methods(unittest.TestCase):
    def setUp(self):
        # having two argparse instances is either tricky or not possible so I'm taking the easy way out
        class FakeArgparser:
            def __init__(self):
                self.quiet = True
        self.filepath = '/tmp/random-path'
        self.args = FakeArgparser

    def test_a1_object_init1(self):
        self.progress_object = CallbackProgress(self.filepath, self.args)
        self.assertIsNotNone(self.progress_object)

    def test_a1_object_init2(self):
        self.progress_object = CallbackProgress(self.filepath, self.args, part=1)
        self.assertIsNotNone(self.progress_object)

    def test_a1_object_init3(self):
        self.progress_object = CallbackProgress(self.filepath, self.args, part=1, total_parts=2)
        self.assertIsNotNone(self.progress_object)

    def test_a2_object_call1(self):
        self.args.quiet = True
        self.progress_object = CallbackProgress(self.filepath, self.args)
        # just testing the following doesn't raise an exception
        self.progress_object(complete=100, total=200)

    def test_a2_object_call2(self):
        self.args.quiet = True
        self.progress_object = CallbackProgress(self.filepath, self.args, part=1, total_parts=2)
        # just testing the following doesn't raise an exception
        self.progress_object(complete=100, total=200)


class test_gen_key_and_salt(unittest.TestCase):
    def setUp(self):
        self.password = 'J#LX2\EZk.@[heal|]O"'
        self.key, self.salt = gen_key_and_salt(password=self.password)

    def test_a1_num_return_elements(self):
        self.assertTrue(len(gen_key_and_salt(password=self.password)) == 2)

    def test_b1_key_is_32_characters(self):
        self.assertEqual(len(self.key), 32)

    def test_c1_salt_is_32_characters(self):
        self.assertEqual(len(self.salt), 32)

    def test_d1_predictable_key_when_using_known_salt(self):
        self.key2, salt2 = gen_key_and_salt(password=self.password, salt=self.salt)
        self.assertEqual(self.key, self.key2)

# test DB functions which are not part of a class
# noinspection PyShadowingBuiltins,PyUnusedLocal
class test_db_operations(unittest.TestCase):
    # noinspection LongLine
    def setUp(self):
        # disable warning and get two tmp names to use
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            self.dbpath = os.tmpnam() + '_db_file'
        # content used to seed the files table
        self.file_table_sql = u'''BEGIN TRANSACTION;
INSERT INTO `files` (filepath,uid,gid,perm_mode,md5,filetype,size,symlink_target,mtime,ctime,encrypted) VALUES ('/test_environments/test1/BLABLA2','1000','1000','0120777','','symlink','2','aa','1435087595','1460924036','0');
INSERT INTO `files` (filepath,uid,gid,perm_mode,md5,filetype,size,symlink_target,mtime,ctime,encrypted) VALUES ('/test_environments/test1/BLABLA3','1000','1000','0120777','','symlink','13','/etc/services','1451387036','1460924036','0');
INSERT INTO `files` (filepath,uid,gid,perm_mode,md5,filetype,size,symlink_target,mtime,ctime,encrypted) VALUES ('/test_environments/test1/empty_test_folder','1000','1000','040755','','dir','4096','','1451238619','1460924036','0');
INSERT INTO `files` (filepath,uid,gid,perm_mode,md5,filetype,size,symlink_target,mtime,ctime,encrypted) VALUES ('/test_environments/test1/test_folder','1000','1000','040755','','dir','4096','','1460924036','1460924036','0');
INSERT INTO `files` (filepath,uid,gid,perm_mode,md5,filetype,size,symlink_target,mtime,ctime,encrypted) VALUES ('/test_environments/test1/test_folder/test_file','1000','1000','0100644','','file','198','','1451238625','1460924036','0');
INSERT INTO `files` (filepath,uid,gid,perm_mode,md5,filetype,size,symlink_target,mtime,ctime,encrypted) VALUES ('/test_environments/test1/test_folder/even_larger_test_file','1000','1000','0100644','','file','115343360','','1454681172','1460924036','0');
INSERT INTO `files` (filepath,uid,gid,perm_mode,md5,filetype,size,symlink_target,mtime,ctime,encrypted) VALUES ('/test_environments/test1/test_folder/large_test_file','1000','1000','0100644','','file','1048576','','1451241993','1460924036','0');
INSERT INTO `files` (filepath,uid,gid,perm_mode,md5,filetype,size,symlink_target,mtime,ctime,encrypted) VALUES ('/test_environments/test1/test_folder/another','1000','1000','040755','','dir','4096','','1460924036','1460924036','0');
INSERT INTO `files` (filepath,uid,gid,perm_mode,md5,filetype,size,symlink_target,mtime,ctime,encrypted) VALUES ('/test_environments/test1/test_folder/folder','1000','1000','040755','','dir','4096','','1460924036','1460924036','0');
INSERT INTO `files` (filepath,uid,gid,perm_mode,md5,filetype,size,symlink_target,mtime,ctime,encrypted) VALUES ('/test_environments/test1/test_folder/another/file2','1000','1000','0100644','','file','5','','1451821701','1460924036','0');
INSERT INTO `files` (filepath,uid,gid,perm_mode,md5,filetype,size,symlink_target,mtime,ctime,encrypted) VALUES ('/test_environments/test1/test_folder/another/file1','1000','1000','0100644','','file','5','','1451821698','1460924036','0');
INSERT INTO `files` (filepath,uid,gid,perm_mode,md5,filetype,size,symlink_target,mtime,ctime,encrypted) VALUES ('/test_environments/test1/test_folder/another/file3','1000','1000','0100644','','file','1048576','','1453064876','1460924036','0');
INSERT INTO `files` (filepath,uid,gid,perm_mode,md5,filetype,size,symlink_target,mtime,ctime,encrypted) VALUES ('/test_environments/test1/test_folder/\u0219i\xee\xfcrich(1)','1000','1000','040755','','dir','4096','','1460924036','1460918307','0');
INSERT INTO `files` (filepath,uid,gid,perm_mode,md5,filetype,size,symlink_target,mtime,ctime,encrypted) VALUES ('/test_environments/test1/test_folder/folder/\u0219i\xee\xfcrich01.jpg','1000','1000','0100664','','file','2986092','','1442783149','1460918277','0');
INSERT INTO `files` (filepath,uid,gid,perm_mode,md5,filetype,size,symlink_target,mtime,ctime,encrypted) VALUES ('/test_environments/test1/test_folder/\u0219i\xee\xfcrich(1)/lalal_file','1000','1000','0100644','','file','0','','1455450621','1460924036','0');
INSERT INTO `files` (filepath,uid,gid,perm_mode,md5,filetype,size,symlink_target,mtime,ctime,encrypted) VALUES ('/test_environments/test1/another_folder','1000','1000','040775','','dir','4096','','1461100122','1461100122','0');
INSERT INTO `files` (filepath,uid,gid,perm_mode,md5,filetype,size,symlink_target,mtime,ctime,encrypted) VALUES ('/test_environments/test1/another_folder2','1000','1000','040775','','dir','4096','','1461100136','1461100136','0');
INSERT INTO `files` (filepath,uid,gid,perm_mode,md5,filetype,size,symlink_target,mtime,ctime,encrypted) VALUES ('/test_environments/test1/another_folder2/amnother_file','1000','1000','0100664','','file','84','','1461100168','1461100168','0');
INSERT INTO `files` (filepath,uid,gid,perm_mode,md5,filetype,size,symlink_target,mtime,ctime,encrypted) VALUES ('/test_environments/test1/another_folder/another_symlink','1000','1000','0120777','','symlink','36','../test_folder/even_larger_test_file','1461273769','1461273769','0');
COMMIT;\n'''

    def tearDown(self):
        try:
            self.dbconn
        except AttributeError:
            pass
        else:
            self.db_conn.close()
        if os.path.exists(self.dbpath):
            os.remove(self.dbpath)

    def test_a1_init_sqlite_db(self):
        init_sqlite_db(self.dbpath)

    def test_b1_get_db_con(self):
        init_sqlite_db(self.dbpath)
        db_conn = return_db_connection(self.dbpath)
        self.assertIsNotNone(db_conn)

    def test_c1_get_db_cursor(self):
        init_sqlite_db(self.dbpath)
        db_conn = return_db_connection(self.dbpath)
        db_cursor = return_db_cursor(db_conn)
        self.assertIsNotNone(db_cursor)

    def test_d1_create_restore_tables(self):
        init_sqlite_db(self.dbpath)
        db_conn = return_db_connection(self.dbpath)
        db_cursor = return_db_cursor(db_conn)
        db_table_restore_files, db_table_restore_settings = create_restore_tables(db_cursor, db_conn, 1451610061)
        self.assertIsNotNone(db_table_restore_files)
        self.assertIsNotNone(db_table_restore_settings)

    # noinspection PyDictCreation
    def test_e1_add_restore_settings_to_restore_session_table(self):
        init_sqlite_db(self.dbpath)
        db_conn = return_db_connection(self.dbpath)
        db_cursor = return_db_cursor(db_conn)
        restore_options = {'restore_list_generated': False,
                           'restore_path': '/tmp/test_folder',
                           'overwrite': True,
                           'overwrite_permissions': True,
                           'file_list_source': 'db'}
        restore_options['db_table_restore_files'], restore_options['db_table_restore_settings'] = create_restore_tables(db_cursor, db_conn, 1451610061)
        add_restore_settings_to_restore_session_table(db_cursor, db_conn, restore_options)

    # db is empty so there won't be any prompt for resuming
    def test_f1_check_and_resume_previous_restore1(self):
        init_sqlite_db(self.dbpath)
        db_conn = return_db_connection(self.dbpath)
        db_cursor = return_db_cursor(db_conn)
        check_and_resume_previous_restore(db_cursor, db_conn)

    # Monkey patch raw_input so it returns 'n' fnd 'n' or the purpose of this test
    # noinspection PyDictCreation
    @patch('s3backuptool.raw_input', side_effect = ['n', 'n'], create=True)
    def test_f1_check_and_resume_previous_restore2(self, input):
        init_sqlite_db(self.dbpath)
        db_conn = return_db_connection(self.dbpath)
        db_cursor = return_db_cursor(db_conn)
        restore_options = {'restore_list_generated': False,
                           'restore_path': '/tmp/test_folder',
                           'overwrite': True,
                           'overwrite_permissions': True,
                           'file_list_source': 'db'}
        restore_options['db_table_restore_files'], restore_options['db_table_restore_settings'] = create_restore_tables(db_cursor, db_conn, 1451610061)
        # seed DB so a prompt will be made
        add_restore_settings_to_restore_session_table(db_cursor, db_conn, restore_options)
        new_restore_options = check_and_resume_previous_restore(db_cursor, db_conn)
        dict_to_compare = {'db_table_restore_settings': None,
                           'db_table_restore_files': None}
        self.assertDictEqual(dict_to_compare, new_restore_options)

    # Monkey patch raw_input so it returns the following answers in sequence for the purpose of this test
    # noinspection PyUnusedLocal
    @patch('s3backuptool.raw_input', side_effect = ['yes', '/tmp', 'yes', 'no', 'db'], create=True)
    def test_g1_get_restore_options(self, input):
        init_sqlite_db(self.dbpath)
        db_conn = return_db_connection(self.dbpath)
        db_cursor = return_db_cursor(db_conn)
	db_cursor.executescript(self.file_table_sql)
	path = '/some/backup/path'
        config_section = 'a_config_section'
        restore_options = get_restore_options(config_section, db_cursor, db_conn, path)
        dict_to_compare = {'file_list_source': 'db',
                           'overwrite': True,
                           'overwrite_permissions': True,
                           'restore_list_generated': False,
                           'restore_path': u'/tmp'}
        self.assertDictContainsSubset(dict_to_compare, restore_options)

    def test_h1_generate_restore_list_from_db(self):
        init_sqlite_db(self.dbpath)
        db_conn = return_db_connection(self.dbpath)
        db_cursor = return_db_cursor(db_conn)
        db_cursor.executescript(self.file_table_sql)
        restore_file_list = generate_restore_list_from_db(db_cursor)
        counter = 0
        for elem in restore_file_list:
            counter += 1
        # there should be 19 elements returned from the DB table files
        self.assertEqual(counter, 19)


class test_validate_config_file(unittest.TestCase):
    def setUp(self):
        # disable warning for insecure filename
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            self.config_file_path = os.tmpnam() + '_config_file'
        config_content = '''#
[DEFAULT]
db_location = /tmp
aws_access_key_id = AWS_KEY_ID
aws_secret_access_key = VERY_SECRET_STUFF
s3_bucket = some-s3-bucket

[section_aaa]
path = /var/some/path
exclude_file = config_file

[section_bbb]
path = /var/some/other/path
encrypt = true
encrypt_password = 390sf234
'''
        with open(self.config_file_path, 'w') as config_file:
            config_file.write(config_content)

    def tearDown(self):
        if os.path.exists(self.config_file_path):
            os.remove(self.config_file_path)
        # enable back logging at WARNING level
        logging.basicConfig(format=log_format, level=logging.WARNING)

    def test_a1_validate_sample_config(self):
        config_file_defaults = {
        }
        config = ConfigParser.SafeConfigParser(config_file_defaults, allow_no_value=False)
        config.read(self.config_file_path)
        self.assertIsNotNone(config)

    def test_b1_validate_configfile_key(self):
        config_file_defaults = {
        }
        config = ConfigParser.SafeConfigParser(config_file_defaults, allow_no_value=False)
        config.read(self.config_file_path)
        for config_section in config.sections():
            validate_configfile_key(config, config_section, 'db_location')

    def test_b2_validate_configfile_key(self):
        config_file_defaults = {
        }
        config = ConfigParser.SafeConfigParser(config_file_defaults, allow_no_value=False)
        config.read(self.config_file_path)
        for config_section in config.sections():
            validate_configfile_key(config, config_section, 'path', allowed_in_default=False)

    def test_b3_validate_configfile_key(self):
        # this test is supposed to test for failure
        config_file_defaults = {
        }
        config = ConfigParser.SafeConfigParser(config_file_defaults, allow_no_value=False)
        config.read(self.config_file_path)
        # disable logging because the next test is supposed to create an ERROR level log entry
        logging.disable(logging.CRITICAL)
        for config_section in config.sections():
            with self.assertRaises(SystemExit):
                validate_configfile_key(config, config_section, 'db_location', allowed_in_default=False)

    def test_c1_validate_config_file(self):
        config = validate_and_return_config_file(self.config_file_path)
        self.assertIsNotNone(config)

    def test_d1_get_config_value(self):
        config = validate_and_return_config_file(self.config_file_path)
        for config_section in config.sections():
            dbpath = get_config_value(config, config_section, 'db_location') + os.sep + config_section + '.db'
            self.assertIsNotNone(dbpath)

    def test_d2_get_config_value(self):
        config = validate_and_return_config_file(self.config_file_path)
        for config_section in config.sections():
            s3_parent_folder_path = get_config_value(config, config_section, 's3_parent_folder_path', default=True, default_value=config_section)
            self.assertEqual(s3_parent_folder_path, config_section)

    def test_d3_get_config_value(self):
        config = validate_and_return_config_file(self.config_file_path)
        for config_section in config.sections():
            check_file_checksum = get_config_value(config, config_section, 'check_file_checksum', default=True, default_value='False', val_type='boolean')
            self.assertFalse(check_file_checksum)

    def test_d4_get_config_value(self):
        config = validate_and_return_config_file(self.config_file_path)
        for config_section in config.sections():
            delete_removed_files = get_config_value(config, config_section, 'delete_removed_files', default=True, default_value='True', val_type='boolean')
            self.assertTrue(delete_removed_files)


# noinspection PyDictCreation
class test_BackedUpItem_methods(unittest.TestCase):
    # noinspection LongLine
    def setUp(self):
        class FakeArgparser:
            def __init__(self):
                self.quiet = True
        self.args = FakeArgparser
        # disable warning for insecure filename
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            self.dbpath = os.tmpnam() + '_db_file'
            self.backedup_dir = os.tmpnam() + '_backedup_dir'
            # next two are for encryption / descryption tests
            self.destination_file = os.tmpnam() + '_encrypted_file'
            self.decrypted_file = os.tmpnam() + '_decrypted_file'
        os.mkdir(self.backedup_dir)
        self.backedup_file = self.backedup_dir + os.sep + 'a_test_file'
        self.root, self.name = self.backedup_file.rsplit(os.sep, 1)
        init_sqlite_db(self.dbpath)
        self.db_conn = return_db_connection(self.dbpath)
        self.db_cursor = return_db_cursor(self.db_conn)
        # content used to seed the files table
        self.file_table_sql = u'''BEGIN TRANSACTION;
        INSERT INTO `files` (filepath,uid,gid,perm_mode,md5,filetype,size,symlink_target,mtime,ctime,encrypted) VALUES ('/test_environments/test1/BLABLA2','1000','1000','0120777','','symlink','2','aa','1435087595','1460924036','0');
        INSERT INTO `files` (filepath,uid,gid,perm_mode,md5,filetype,size,symlink_target,mtime,ctime,encrypted) VALUES ('/test_environments/test1/BLABLA3','1000','1000','0120777','','symlink','13','/etc/services','1451387036','1460924036','0');
        INSERT INTO `files` (filepath,uid,gid,perm_mode,md5,filetype,size,symlink_target,mtime,ctime,encrypted) VALUES ('/test_environments/test1/empty_test_folder','1000','1000','040755','','dir','4096','','1451238619','1460924036','0');
        INSERT INTO `files` (filepath,uid,gid,perm_mode,md5,filetype,size,symlink_target,mtime,ctime,encrypted) VALUES ('/test_environments/test1/test_folder','1000','1000','040755','','dir','4096','','1460924036','1460924036','0');
        INSERT INTO `files` (filepath,uid,gid,perm_mode,md5,filetype,size,symlink_target,mtime,ctime,encrypted) VALUES ('/test_environments/test1/test_folder/test_file','1000','1000','0100644','','file','198','','1451238625','1460924036','0');
        INSERT INTO `files` (filepath,uid,gid,perm_mode,md5,filetype,size,symlink_target,mtime,ctime,encrypted) VALUES ('/test_environments/test1/test_folder/even_larger_test_file','1000','1000','0100644','','file','115343360','','1454681172','1460924036','0');
        INSERT INTO `files` (filepath,uid,gid,perm_mode,md5,filetype,size,symlink_target,mtime,ctime,encrypted) VALUES ('/test_environments/test1/test_folder/large_test_file','1000','1000','0100644','','file','1048576','','1451241993','1460924036','0');
        INSERT INTO `files` (filepath,uid,gid,perm_mode,md5,filetype,size,symlink_target,mtime,ctime,encrypted) VALUES ('/test_environments/test1/test_folder/another','1000','1000','040755','','dir','4096','','1460924036','1460924036','0');
        INSERT INTO `files` (filepath,uid,gid,perm_mode,md5,filetype,size,symlink_target,mtime,ctime,encrypted) VALUES ('/test_environments/test1/test_folder/folder','1000','1000','040755','','dir','4096','','1460924036','1460924036','0');
        INSERT INTO `files` (filepath,uid,gid,perm_mode,md5,filetype,size,symlink_target,mtime,ctime,encrypted) VALUES ('/test_environments/test1/test_folder/another/file2','1000','1000','0100644','','file','5','','1451821701','1460924036','0');
        INSERT INTO `files` (filepath,uid,gid,perm_mode,md5,filetype,size,symlink_target,mtime,ctime,encrypted) VALUES ('/test_environments/test1/test_folder/another/file1','1000','1000','0100644','','file','5','','1451821698','1460924036','0');
        INSERT INTO `files` (filepath,uid,gid,perm_mode,md5,filetype,size,symlink_target,mtime,ctime,encrypted) VALUES ('/test_environments/test1/test_folder/another/file3','1000','1000','0100644','','file','1048576','','1453064876','1460924036','0');
        INSERT INTO `files` (filepath,uid,gid,perm_mode,md5,filetype,size,symlink_target,mtime,ctime,encrypted) VALUES ('/test_environments/test1/test_folder/\u0219i\xee\xfcrich(1)','1000','1000','040755','','dir','4096','','1460924036','1460918307','0');
        INSERT INTO `files` (filepath,uid,gid,perm_mode,md5,filetype,size,symlink_target,mtime,ctime,encrypted) VALUES ('/test_environments/test1/test_folder/folder/\u0219i\xee\xfcrich01.jpg','1000','1000','0100664','','file','2986092','','1442783149','1460918277','0');
        INSERT INTO `files` (filepath,uid,gid,perm_mode,md5,filetype,size,symlink_target,mtime,ctime,encrypted) VALUES ('/test_environments/test1/test_folder/\u0219i\xee\xfcrich(1)/lalal_file','1000','1000','0100644','','file','0','','1455450621','1460924036','0');
        INSERT INTO `files` (filepath,uid,gid,perm_mode,md5,filetype,size,symlink_target,mtime,ctime,encrypted) VALUES ('/test_environments/test1/another_folder','1000','1000','040775','','dir','4096','','1461100122','1461100122','0');
        INSERT INTO `files` (filepath,uid,gid,perm_mode,md5,filetype,size,symlink_target,mtime,ctime,encrypted) VALUES ('/test_environments/test1/another_folder2','1000','1000','040775','','dir','4096','','1461100136','1461100136','0');
        INSERT INTO `files` (filepath,uid,gid,perm_mode,md5,filetype,size,symlink_target,mtime,ctime,encrypted) VALUES ('/test_environments/test1/another_folder2/amnother_file','1000','1000','0100664','','file','84','','1461100168','1461100168','0');
        INSERT INTO `files` (filepath,uid,gid,perm_mode,md5,filetype,size,symlink_target,mtime,ctime,encrypted) VALUES ('/test_environments/test1/another_folder/another_symlink','1000','1000','0120777','','symlink','36','../test_folder/even_larger_test_file','1461273769','1461273769','0');
        COMMIT;\n'''
        # see files table
        self.db_cursor.executescript(self.file_table_sql)
        # table needed to test restore related functions and methods
        self.restore_time = 1451610061
        self.restore_options = {'restore_list_generated': False,
                                'restore_path': '/tmp/test_folder',
                                'overwrite': True,
                                'overwrite_permissions': True,
                                'file_list_source': 'db'}
        self.restore_metadata = {'ctime': u'1460918277',
                                 'encrypted': False,
                                 'filename_encoded': False,
                                 'filepath': u'/test_environments/test1/test_folder/folder/\u0219i\xee\xfcrich01.jpg',
                                 'filetype': u'file',
                                 'gid': u'1000',
                                 'mtime': u'1442783149',
                                 'perm_mode': u'0100664',
                                 'size': u'2986092',
                                 'uid': u'1000'}
        self.restore_options['db_table_restore_files'], self.restore_options['db_table_restore_settings'] = create_restore_tables(
            self.db_cursor, self.db_conn, self.restore_time)
        add_restore_settings_to_restore_session_table(self.db_cursor, self.db_conn, self.restore_options)
        self.config_section = 'some_config_section'
        self.s3_bucket_obj = None
        self.s3_parent_folder_path = self.config_section
        self.check_file_checksum = False
        self.encrypt = False
        self.encrypt_password = '^h|\uGIj[RF5Afv>VC7_'

    def tearDown(self):
        try:
            self.dbconn
        except AttributeError:
            pass
        else:
            self.db_conn.close()
        if os.path.exists(self.dbpath):
            os.remove(self.dbpath)
        if os.path.exists(self.backedup_dir):
            shutil.rmtree(self.backedup_dir)
        if os.path.exists(self.destination_file):
            os.remove(self.destination_file)
        if os.path.exists(self.decrypted_file):
            os.remove(self.decrypted_file)
        # enable back logging at WARNING level
        logging.basicConfig(format=log_format, level=logging.WARNING)

    # test init for file items
    def test_a1_BackedUpItem_init_file1(self):
        item_to_process = BackedUpItem(self.root, self.name, self.db_cursor, self.db_conn, 'file', self.s3_bucket_obj, self.s3_parent_folder_path, 
                                       self.check_file_checksum, self.config_section, self.args, self.encrypt, self.encrypt_password)
        self.assertIsNotNone(item_to_process)

    # test init for file items prepared for restore
    def test_a1_BackedUpItem_init_file2(self):
        item_to_process = BackedUpItem(self.root, self.name, self.db_cursor, self.db_conn, 'file', self.s3_bucket_obj, self.s3_parent_folder_path,
                                       self.check_file_checksum, self.config_section, self.args, self.encrypt, self.encrypt_password,
                                       self.restore_options, restore_metadata=self.restore_metadata)
        self.assertIsNotNone(item_to_process)

    def test_a1_BackedUpItem_init_dir(self):
        self.root, self.name = self.backedup_dir.rsplit(os.sep, 1)
        item_to_process = BackedUpItem(self.root, self.name, self.db_cursor, self.db_conn, 'dir', self.s3_bucket_obj, self.s3_parent_folder_path,
                                       self.check_file_checksum, self.config_section, self.args, self.encrypt, self.encrypt_password)
        self.assertIsNotNone(item_to_process)

    def prepare_for_encryption(self):
        # generate key, salt and IV
        self.key, self.salt = gen_key_and_salt(password=self.encrypt_password)
        # Initialization Vector needs to be 16 bytes when using AES
        self.iv = os.urandom(16)
        self.encryptor = AES.new(self.key, AES.MODE_CBC, self.iv)
        # seed 1 MiB file
        with open(self.backedup_file, 'wb') as in_file:
            in_file.write(os.urandom(1048576))

    def test_b1_file_encryption_in_one_run(self):
        self.prepare_for_encryption()
        item_to_process = BackedUpItem(self.root, self.name, self.db_cursor, self.db_conn, 'file', self.s3_bucket_obj, self.s3_parent_folder_path,
                                       self.check_file_checksum, self.config_section, self.args, self.encrypt, self.encrypt_password)
        encrypted_file = item_to_process.encrypt_file(encryptor=self.encryptor, salt=self.salt, iv=self.iv,
                                                      chunksize=os.path.getsize(self.backedup_file.encode('utf-8')))
        with open(self.destination_file, 'wb') as out_file:
            out_file.write(encrypted_file.read())
        self.assertTrue(os.path.isfile(self.destination_file))

    def DISABLED_test_b1_file_encryption_and_decryption(self):
        self.encrypt_file_in_one_run()
        item_to_process = BackedUpItem(self.root, self.name, self.db_cursor, self.db_conn, 'file', self.s3_bucket_obj, self.s3_parent_folder_path,
                                       self.check_file_checksum, self.config_section, self.args, self.encrypt, self.encrypt_password)
        encrypted_file = item_to_process.encrypt_file(encryptor=self.encryptor, salt=self.salt, iv=self.iv,
                                                      chunksize=os.path.getsize(self.backedup_file.encode('utf-8')))
        with open(self.destination_file, 'wb') as out_file:
            out_file.write(encrypted_file.read())
        # test decryption

    def test_c1_calculate_md5checksum_1(self):
        file_content_to_test = '''Deip2hie ohSees6v Phoh9eiT Ia1rair7 aL3ahhue Paegh2hu gie5uToh ho0peWai
Soajei4i ohRohwi1 vae5Ohng kiw1Aiqu aiCha9oo zai2Boh8 mieGh4in Iebu1nae
hai9Dohw Ahjai9Tu ri0aeJie yePhoh0a Sethoo3s eRoh6oov bain3Phi ad0Loaze
Dasee3ah ohna7Bon uZ4aeLaY Eexe9ies sij7eiKo eet2Kaeh An3duom0 aeP6shoh
Ahyi7Dai Xiel9daJ pe3Eephe Eith0aey Cee5thiv leu5ooRi ixaMoo8i ooxi7She
ga1Fah6e aifeiC7k nuk9oe4U AiG0ohge ue9ieZah No6abahr ohCh0ooB Weer8ah0
aiSh9aih faiveeG6 iaMaem5f iev0iJ6o Ei9Epaib Ai9Ooth6 aht6MahH eeg5ohTi
Ahgei8en AeRies8o Air3doh0 poP1Sais Siehoh1i Nee0Bohm hoK7wi0a Eotah2wi
iey2haeH nooRaeB4 oaXohf5b ieV8ush4 oYae8Aho VooW0via Quei0bef yei5sieL
fa7Ro1th Yoh3eeyo PieN4nax eeCagh6x ke0mu8Ee ieBoZah9 quef8OhZ eeMingu8
Paekee6o aeYeev8B Io6aph1g Dahgh6ei ubie5aeF Nee9Feep Ue3Uthah ohf2Ahwu
cahpaNg9 aequaj6U ahsoh4Ye Vied0ooc aiNongu8 aht4Phee kooP9aph iR2xu1oi
aeng3Eib ohyei0Ph doo3maeZ Leixif9i rie7au2O oon1Mee1 eay9boPh Ixoo7oid
xui3koYa Ee0Lahwi ahph5Bae eeJu8joh ahQu7eeC Dahng4ke fah6Ithi Oe5phiof
gai9ieGh do3Theix eb3yaJah Ithe5lie xoh0Iequ eesuTei9 pee4Equo ieR9ieda
Eiyoo8Ej aiCh6igo Duy9phei aeH6vi2g eephahC1 ith2ahPh Giugot6h Fupaih4o
Thoh8Fai Eiv2yei0 aiteex0T ieP6eicu zieH0pei oerei6Qu woo4Eeth aTohz0eo
Ceiph2ju Shienga9 poa7tohG beeZ2Aix Gau1Equo leiZ0tur Hae2Pail Thie1mai
dohL6Ool enee4Ii7 ieng6Woo auQuux6z Jeit5aet Po7eegei eiroh9Go puij6Ohx
ia4loWuv mo1eeGoK zo0aiYei aeJ2phoo Eimao6ee No0joh8W Aeng3Aep eewah9Mo\n'''
        md5sum_to_compare = 'b83853ba572d77cd1cefa449891fe7e7'
        with open(self.backedup_file, 'w') as out_file:
            out_file.write(file_content_to_test)
        item_to_process = BackedUpItem(self.root, self.name, self.db_cursor, self.db_conn, 'file', self.s3_bucket_obj, self.s3_parent_folder_path,
                                       self.check_file_checksum, self.config_section, self.args, self.encrypt, self.encrypt_password)
        item_to_process.calculate_md5checksum()
        self.assertEqual(md5sum_to_compare, item_to_process.file_md5_sum)

    def test_c1_calculate_md5checksum_2(self):
        file_content_to_test = '''Deip2hie ohSees6v Phoh9eiT Ia1rair7 aL3ahhue Paegh2hu gie5uToh ho0peWai
Soajei4i ohRohwi1 vae5Ohng kiw1Aiqu aiCha9oo zai2Boh8 mieGh4in Iebu1nae
hai9Dohw Ahjai9Tu ri0aeJie yePhoh0a Sethoo3s eRoh6oov bain3Phi ad0Loaze
Dasee3ah ohna7Bon uZ4aeLaY Eexe9ies sij7eiKo eet2Kaeh An3duom0 aeP6shoh
Ahyi7Dai Xiel9daJ pe3Eephe Eith0aey Cee5thiv leu5ooRi ixaMoo8i ooxi7She
ga1Fah6e aifeiC7k nuk9oe4U AiG0ohge ue9ieZah No6abahr ohCh0ooB Weer8ah0
aiSh9aih faiveeG6 iaMaem5f iev0iJ6o Ei9Epaib Ai9Ooth6 aht6MahH eeg5ohTi
Ahgei8en AeRies8o Air3doh0 poP1Sais Siehoh1i Nee0Bohm hoK7wi0a Eotah2wi
iey2haeH nooRaeB4 oaXohf5b ieV8ush4 oYae8Aho VooW0via Quei0bef yei5sieL
fa7Ro1th Yoh3eeyo PieN4nax eeCagh6x ke0mu8Ee ieBoZah9 quef8OhZ eeMingu8
Paekee6o aeYeev8B Io6aph1g Dahgh6ei ubie5aeF Nee9Feep Ue3Uthah ohf2Ahwu
cahpaNg9 aequaj6U ahsoh4Ye Vied0ooc aiNongu8 aht4Phee kooP9aph iR2xu1oi
aeng3Eib ohyei0Ph doo3maeZ Leixif9i rie7au2O oon1Mee1 eay9boPh Ixoo7oid
xui3koYa Ee0Lahwi ahph5Bae eeJu8joh ahQu7eeC Dahng4ke fah6Ithi Oe5phiof
gai9ieGh do3Theix eb3yaJah Ithe5lie xoh0Iequ eesuTei9 pee4Equo ieR9ieda
Eiyoo8Ej aiCh6igo Duy9phei aeH6vi2g eephahC1 ith2ahPh Giugot6h Fupaih4o
Thoh8Fai Eiv2yei0 aiteex0T ieP6eicu zieH0pei oerei6Qu woo4Eeth aTohz0eo
Ceiph2ju Shienga9 poa7tohG beeZ2Aix Gau1Equo leiZ0tur Hae2Pail Thie1mai
dohL6Ool enee4Ii7 ieng6Woo auQuux6z Jeit5aet Po7eegei eiroh9Go puij6Ohx
ia4loWuv mo1eeGoK zo0aiYei aeJ2phoo Eimao6ee No0joh8W Aeng3Aep eewah9Mo\n'''
        md5_base64_digest_to_compare = 'uDhTulctd80c76RJiR/n5w=='
        with open(self.backedup_file, 'w') as out_file:
            out_file.write(file_content_to_test)
        item_to_process = BackedUpItem(self.root, self.name, self.db_cursor, self.db_conn, 'file', self.s3_bucket_obj, self.s3_parent_folder_path,
                                       self.check_file_checksum, self.config_section, self.args, self.encrypt, self.encrypt_password)
        item_to_process.calculate_md5checksum()
        self.assertEqual(md5_base64_digest_to_compare, item_to_process.file_md5_base64_digest)

if __name__ == '__main__':
    arguments = get_args()
    if arguments.verbose:
        verbosity = 2
    else:
        verbosity = 1

    log_format = '%(levelname)s: %(message)s'
    logging.basicConfig(format=log_format, level=logging.WARNING)

    suite_CallbackProgress_methods = unittest.TestLoader().loadTestsFromTestCase(test_CallbackProgress_methods)
    unittest.TextTestRunner(verbosity=verbosity).run(suite_CallbackProgress_methods)

    suite_db_operations = unittest.TestLoader().loadTestsFromTestCase(test_db_operations)
    unittest.TextTestRunner(verbosity=verbosity).run(suite_db_operations)

    suite_validate_config_file = unittest.TestLoader().loadTestsFromTestCase(test_validate_config_file)
    unittest.TextTestRunner(verbosity=verbosity).run(suite_validate_config_file)

    suite_gen_key_and_salt = unittest.TestLoader().loadTestsFromTestCase(test_gen_key_and_salt)
    unittest.TextTestRunner(verbosity=verbosity).run(suite_gen_key_and_salt)

    suite_BackedUpItem_methods = unittest.TestLoader().loadTestsFromTestCase(test_BackedUpItem_methods)
    unittest.TextTestRunner(verbosity=verbosity).run(suite_BackedUpItem_methods)
